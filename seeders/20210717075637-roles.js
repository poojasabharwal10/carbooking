'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Roles', [{
        name: 'User',
        description: 'User',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Cab Driver',
        description: 'Cab Driver',
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Roles', null, {});
  }
};
