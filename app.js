const express = require('express')
const app = express()
var bodyParser = require('body-parser')
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
import { apiRouter } from './src/api/index'
// import { authenticateToken } from './src/api/helpers/userAuthorization'
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
require('dotenv').config()

const port = process.env.PORT ? process.env.PORT : 3001

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

app.use(passport.initialize());
app.use(passport.session());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.get('/', (req, res) => res.send('Hello World!'))
app.use('/api', apiRouter)
app.listen(port, () => console.log(`Example app listening on port ${port}!`))