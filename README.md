
## Clone a repository

Use these steps to start code.

1. After cloning run ```npm i``` cmd.
2. Run ```npx sequelize-cli db:migrate``` cmd for mysql migrations.
3. Start the project through ```npm start```.
4. Swagger link <localurl>/api-docs.
