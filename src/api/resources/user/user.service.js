const Bcrypt = require("bcryptjs")
const Users = require('../../../../models').Users
const Addresses = require('../../../../models').Addresses
const Persons = require('../../../../models').Persons
const Roles = require('../../../../models').Roles
const Availabilities = require('../../../../models').Availabilities
import jwt from '../../helpers/jwt'

export default {
  /*
  *
  * Service for Employee sign up
  * 
  * return data
  * 
  */
  async signup(body) {
    return new Promise((resolve, reject) => {
      var res = {
        success: false
      }
      try {
        body.email = body.email.toLowerCase().trim()
        body.userDetails = {
          firstName: body.firstName
        }
        body.userAddress = {
          addressLine1: body.addressLine1,
          city: body.city,
          state: body.state,
          zip: body.zip
        }
        if(body.addressLine2) {
          body.userAddress.addressLine2 = body.addressLine2
        }
        if(body.latitude) {
          body.userAddress.latitude = body.latitude
        }
        if(body.longitude) {
          body.userAddress.longitude = body.longitude
        }
        if(body.dob) {
          body.userDetails.dob = body.dob
        }
        if(body.lastName) {
          body.userDetails.lastName = body.lastName
        }
        if(body.gender) {
          body.userDetails.gender = body.gender
        }
        if(body.profilePic) {
          body.userDetails.profilePic = body.profilePic
        }
        if(body.phoneNumber) {
          body.userDetails.phoneNumber = body.phoneNumber
        }
        if(body.availability) {
          body.userAvailability.availability = 'yes'
        }
        Users.findOne({
          where: {
            userName: body.userName.toLowerCase().trim()
          },
          include: [{
            model: Persons,
            as: 'userDetails'
          }, {
            model: Addresses,
            as: 'userAddress'
          }, {
            model: Availabilities,
            as: 'userAvailability'
          }]
        }).then(user => {
          if (user) {
            if(user.deletedAt == null) {
              if(user.email == body.email) {
                res.message = 'Email already exists.'
                res.code = 'BAD_REQUEST'
                reject(res)
              } else {
                res.message = 'User name already exists.'
                reject(res)
              }
            } else {
              body.deletedAt = null
              user.update(body,{
                include: [{
                  model: Persons,
                  as: 'userDetails'
                }, {
                  model: Addresses,
                  as: 'userAddress'
                }]
              }).then(employee => {
                res.success = true
                res.data = user
                res.message = 'Register successfully.'
                resolve(res)
              }).catch(err => {
                res.message = 'Something went wrong.'
                res.error = err
                reject(res)
              })
            }
          } else {
            Users.create(body,{
              include: [{
                model: Persons,
                as: 'userDetails'
              }, {
                model: Addresses,
                as: 'userAddress'
              }]
            }).then(employee => {
              res.success = true
              res.data = employee
              res.message = 'Register successfully.'
              resolve(res)
            }).catch(err => {
              res.message = 'Something went wrong.'
              res.error = err
              reject(res)
            })
          }
        }).catch(err => {
          res.message = 'Something went wrong.'
          res.error = err
          reject(res)
        })
      } catch (err) {
        res.message = 'Something went wrong.'
        res.error = err
        reject(res)
      }
    })
  },
  /*
  *
  * Service for Employee login
  * 
  * Params: [userName,password]
  * 
  * return data
  * 
  */
  async login(body) {
    return new Promise((resolve, reject) => {
      var res = {
        success: false
      }
      try {
        Users.findOne({
          where: {
            userName: body.userName.toLowerCase().trim()
          }
        }).then(user => {
          if (user) {
            console.log('users....',user)
            if (Bcrypt.compareSync(body.password, user.password)) {
              body.userId = user.id
              res.success = true
              res.token = jwt.issue({ id: user.id }, '1d'),
              res.user = user
              res.message = 'Login successfully'
              resolve(res)
            } else {
              res.message = 'Wrong password.'
              // res.error = err
              reject(res)
            }
          } else {
            res.message = 'User name does not exists.'
            // res.error = err
            reject(res)
          }
        }).catch(err => {
          console.log(err)
          res.message = 'Something went wrong.'
          res.error = err
          reject(res)
        })
      } catch (err) {
        res.message = 'Something went wrong.'
        res.error = err
        reject(res)
      }
    })
  },
/*
  *
  * Service for roles
  * 
  * return data
  * 
  */
 async roles(usrs) {
  return new Promise((resolve, reject) => {
    var res = {
      success: false
    }
    try {
      Roles.findAll({
        attributes: ['id', 'name']
      }).then(roles => {
        if (roles) {
            res.success = true
            res.roles = roles
            res.message = 'Roles list'
            resolve(res)
        } else {
          res.message = 'Data not found.'
          // res.error = err
          reject(res)
        }
      }).catch(err => {
        console.log(err)
        res.message = 'Something went wrong.'
        res.error = err
        reject(res)
      })
    } catch (err) {
      console.log(err)
      res.message = 'Something went wrong.'
      res.error = err
      reject(res)
    }
  })
},

}
