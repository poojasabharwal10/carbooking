import Joi from 'joi'
import bcrypt from 'bcryptjs'

export default {
    encryptPassword(palinText) {
        const salt = bcrypt.genSaltSync(10)
        return bcrypt.hashSync(palinText, salt)
    },
    comparePassword(plainText, encrypedPassword) {
        return bcrypt.compareSync(plainText, encrypedPassword)
    },
    validateSignup(body) {
        const schema = Joi.object().keys({
            roleId: Joi.number().required().label('Role'),
            userName: Joi.string().required().label('User name'),
            password: Joi.string().required().label('Password'),
            firstName: Joi.string().required().label('First name'),
            lastName: Joi.string().required().label('Last name'),
            email: Joi.string().email().required().label('Email'),
            gender: Joi.string().allow('').label('Gender'),
            phoneNumber: Joi.string().allow('').label('Phone number'),
            bio: Joi.string().allow('').label('Bio'),
            addressLine1: Joi.string().required().label('Address line 1'),
            addressLine2: Joi.string().allow('').label('Address line 2'),
            city: Joi.string().required().label('City'),
            state: Joi.string().required().label('State'),
            zip: Joi.string().required().label('Zip'),
            latitude: Joi.string().allow('').label('Latitude'),
            longitude: Joi.string().allow('').label('Longitude')
        })
        const { value, error } = Joi.validate(body, schema, {abortEarly: false})
        if (error && error.details) {
            let data = {error: true, details: error.details }
            return data
        } else {
            return value
        }
    },
    validateLogin(body) {
        const schema = Joi.object().keys({
            userName: Joi.string().required().label('User name'),
            password: Joi.string().required().label('Password')
        })
        const { value, error } = Joi.validate(body, schema, {abortEarly: false})
        if (error && error.details) {
            let data = {error: true, message: error.details[0].message, details: error.details }
            return data
        } else {
            return value
        }
    }
}