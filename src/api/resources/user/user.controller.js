import userValidation from './user.validation'
import userService from './user.service'
var HttpStatus = require('http-status-codes')
// const requestIp = require('request-ip')
// var colors = require('colors/safe')
var multer = require('multer')
var path = require('path')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/images/profile')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
})
var upload= multer({ storage: storage}).single('profilePic');

export default {

    /*
    *
    * Api for Employee sign up
    * 
    * Params: [userName,firstName,lastName,email,phone,roleId,password,dob,designation]
    * 
    * return data
    * 
    */
    async signup(req, res) {
        try {
                    const validates = await userValidation.validateSignup(req.body)
                    if (validates.error == true) {
                        return res.status(400).json(validates).end()
                    }
                    await userService.signup(req.body).then(response => {
                        return res.status(HttpStatus.OK).send(response)
                    }).catch(error => {
                        if (error.error) {
                            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error)
                        } else if (error.code) {
                            return res.status(HttpStatus.BAD_REQUEST).send(error)
                        } else {
                            return res.status(HttpStatus.NOT_FOUND).send(error)
                        }
                    })
        } catch (err) {
            return res.status(500).send(err)
        }
    },

    /*
    *
    * Api for Employee login
    * 
    * Params: [userName,password]
    * 
    * return data
    * 
    */
    async login(req, res) {
        try {
            const validates = await userValidation.validateLogin(req.body)
            if (validates.error == true) {                    /// if validations failed
                return res.status(400).json(validates).end()
            }
            await userService.login(validates).then(response => {
                return res.status(HttpStatus.OK).send(response)
            }).catch(error => {
                if (error.error) {
                    return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error)
                } else if (error.code) {
                    return res.status(HttpStatus.BAD_REQUEST).send(error)
                } else {
                    return res.status(HttpStatus.NOT_FOUND).send(error)
                }
            })
        } catch (err) {
            return res.status(500).send(err)
        }
    },


    /*
    *
    * Api for get Roles
    * 
    * return roles
    * 
    */
    async roles(req, res) {
        try {
            await userService.roles().then(response => {
                return res.status(HttpStatus.OK).send(response)
            }).catch(error => {
                return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error)
            })
        } catch (err) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(err)
        }
    },


}