import bookingValidation from './booking.validation'
import bookingService from './booking.service'
var HttpStatus = require('http-status-codes')
export default {

    /*
    *
    * Api for find car
    * 
    * return cars list
    * 
    */
    async findCar(req, res) {
        try {
            console.log(req.user.id)
            await bookingService.findCar(req.user.id).then(response => {  //req.user.id
                return res.status(HttpStatus.OK).send(response)
            }).catch(error => {
                if (error.code) {
                    return res.status(HttpStatus.BAD_REQUEST).send(error)
                } else {
                    return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error)
                }
            })
        } catch (err) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(err)
        }
    }


}