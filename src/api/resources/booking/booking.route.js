import express from 'express'
import passport from 'passport'
import { authenticateToken } from './../../helpers/userAuthorization'
import bookingController from './booking.controller'

export const bookingRouter = express.Router()

bookingRouter.get('/find_car',authenticateToken, bookingController.findCar)  