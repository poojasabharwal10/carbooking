const Users = require('../../../../models').Users
const Addresses = require('../../../../models').Addresses
const Persons = require('../../../../models').Persons
const Roles = require('../../../../models').Roles
const Availabilities = require('../../../../models').Availabilities
const Sequelize = require('sequelize')

export default {
 
/*
  *
  * Service for find car
  * 
  * Params: [userId]
  * 
  * return data
  * 
  */
 async findCar(id) {
  return new Promise((resolve, reject) => {
    var res = {
      success: false
    }
    try {
      Users.findAll({
        where: { id:id},
        include: [{
          model: Roles,
          as: 'userRole',
          where: {name : 'Cab Driver'}
        }, {
          model: Persons,
          as: 'userDetails'
        }, {
          model: Addresses,
          as: 'userAddress',
          where: Sequelize.where(
            Sequelize.literal(
             ' 3959 * acos (cos ( radians(78.3232) )* cos( radians( latitude ) ) * cos( radians( longitude ) - radians(65.3234) )+ sin ( radians(78.3232) ) * sin( radians( latitude ) )) >= 30'
            ))
        }, {
          model: Availabilities,
          as: 'userAvailability',
          where: {
            availability: 'yes'
          }
        }]
      }).then(user => {
        if (user) {
            res.success = true
            res.user = user
            res.message = 'Near by cars.'
            resolve(res)
        } else {
          res.message = 'Data not found.'
          // res.error = err
          reject(res)
        }
      }).catch(err => {
        console.log(err)
        res.message = 'Something went wrong.'
        res.error = err
        reject(res)
      })
    } catch (err) {
      console.log(err)
      res.message = 'Something went wrong.'
      res.error = err
      reject(res)
    }
  })
}
}
