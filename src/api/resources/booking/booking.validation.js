import Joi from 'joi'

export default {
    validateLogin(body) {
        const schema = Joi.object().keys({
            userName: Joi.string().required().label('User name'),
            password: Joi.string().required().label('Password')
        })
        const { value, error } = Joi.validate(body, schema, {abortEarly: false})
        if (error && error.details) {
            let data = {error: true, message: error.details[0].message, details: error.details }
            return data
        } else {
            return value
        }
    }
}