const express = require('express')
import { userRouter } from './resources/user/user.route'
import { bookingRouter } from './resources/booking/booking.route'
export const apiRouter = express.Router()

apiRouter.use('/',userRouter) 
apiRouter.use('/booking',bookingRouter) 