
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const jwt = require('jsonwebtoken');
const Users = require('../../../models').Users
const Addresses = require('../../../models').Addresses
const Persons = require('../../../models').Persons
const Roles = require('../../../models').Roles

export function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.SECRET, async (err, data) => {
    if (err) return res.sendStatus(403)    
    let id = data.id
    try {
      console.log('ssss')
              await Users.findOne({
                where: {
                  id: id
                },
                include: [{
                  model: Roles,
                  as: 'userRole',
                  where: {name : 'User'}
                }, {
                  model: Persons,
                  as: 'userDetails'
                }, {
                  model: Addresses,
                  as: 'userAddress',
                }]
              }).then(user => {
                // console.log('user.....',user)
                if (user) {
                  req.user = user
                  next()
                } else {
                  return res.sendStatus(401)
                }
              }).catch(err => {          
                return res.sendStatus(401)
              })
              //Send the user information to the next middleware
              
            } catch (error) {
              return res.sendStatus(401)
            }
  })
}
// export function user() {
// //   const opts = {};
// // opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
// // opts.secretOrkey = keys.secretOrkey;
//   // console.log('gvhghjg',opts)
//    passport.use('user', new localStrategy({
//     id: 'id'
//   }, async (id, done) => {
//       try {
//         //Save the information provided by the user to the the database
//         const user = Users.findOne({
//           where: {
//             id: id
//           },
//           include: [{
//             model: Roles,
//             as: 'userRole',
//             where: {name : 'User'}
//           }, {
//             model: Persons,
//             as: 'userDetails'
//           }, {
//             model: Addresses,
//             as: 'userAddress',
//           }]
//         }).then(user => {
//           if (user) {
//             return done(null, user);
//           } else {
//             return done('null', null);
//           }
//         }).catch(err => {          
//           return done(err, null);
//         })
//         //Send the user information to the next middleware
        
//       } catch (error) {
//         done(error);
//       }
//   }))
// }