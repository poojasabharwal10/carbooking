'use strict';
module.exports = (sequelize, DataTypes) => {
  const Availabilities = sequelize.define('Availabilities', {
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      },
    },
    availability: {
      type: DataTypes.ENUM('YES','No')
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {});
  Availabilities.associate = function(models) {
    // associations can be defined here
  };
  return Availabilities;
};