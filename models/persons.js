'use strict';
module.exports = (sequelize, DataTypes) => {
  const Persons = sequelize.define('Persons', {
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      },
      allowNull: false
    },
    firstName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    lastName: {
      allowNull: true,
      type: DataTypes.STRING
    },
    gender: {
      type: DataTypes.ENUM('Male', 'Female', 'Other'),
      defaultValue: 'Male',
      allowNull : false
    },
    phoneNumber: {
      allowNull: true,
      type: DataTypes.STRING
    },
    profilePicture: {
      allowNull: true,
      type: DataTypes.STRING
    },
    bio: {
      allowNull: true,
      type: DataTypes.TEXT
    }
  }, {});
  Persons.associate = function(models) {
    // associations can be defined here
  };
  return Persons;
};