'use strict';
const bcrypt = require("bcryptjs");
module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {     
    roleId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Roles',
        key: 'id'
      },
      allowNull: false
    },   
    email: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING
    },
    userName: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    isDeleted: {
      type: DataTypes.ENUM('YES','No'),
      allowNull: false,
      defaultValue: 'No'
    },
  }, {});
  Users.associate = function(models) {
    Users.belongsTo(models.Roles, {
      foreignKey : 'roleId',
      targetKey: 'id',
      as: 'userRole'
    });
    Users.hasMany(models.Persons, {
      foreignKey : 'userId',
      targetKey: 'id',
      as: 'userDetails'
    });
    Users.hasMany(models.Addresses, {
      foreignKey : 'userId',
      targetKey: 'id',
      as: 'userAddress'
    });
    Users.hasMany(models.Availabilities, {
      foreignKey : 'userId',
      targetKey: 'id',
      as: 'userAvailability'
    });
// associations can be defined here
  };
  Users.beforeCreate((user, options) => {
    return bcrypt.hash(user.password, 10)
        .then(hash => {
            user.password = hash;
        })
        .catch(err => {
            throw new Error(); 
        });
  });
  Users.beforeUpdate((user, options) => {
    return bcrypt.hash(user.password, 10)
          .then(hash => {
              user.password = hash;
          })
          .catch(err => { 
              throw new Error(); 
          });
  });
  return Users;
};