'use strict';
module.exports = (sequelize, DataTypes) => {
  const Addresses = sequelize.define('Addresses', {    
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      },
      allowNull: false
    },
    addressLine1: {
      allowNull: false,
      type: DataTypes.STRING
    },
    addressLine2: {
      allowNull: true,
      type: DataTypes.STRING
    },
    city: {
      allowNull: false,
      type: DataTypes.STRING
    },
    state: {
      allowNull: false,
      type: DataTypes.STRING
    },
    zip: {
      allowNull: false,
      type: DataTypes.STRING
    },
    latitude: {
      allowNull: true,
      type: DataTypes.STRING
    },
    longitude: {
      allowNull: true,
      type: DataTypes.STRING
    },
    createdBy: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      },
      allowNull: true
    }
  }, {});
  Addresses.associate = function(models) {
    // associations can be defined here
  };
  return Addresses;
};