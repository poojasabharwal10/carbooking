'use strict';
module.exports = (sequelize, DataTypes) => {
  const Logins = sequelize.define('Logins', {    
    token: {
      type: DataTypes.STRING
    },
    deviceId: {
      type: DataTypes.STRING
    },
    deviceType: {
      type: DataTypes.STRING
    },
    rememberToken: {
      type: DataTypes.STRING
    },
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      },
      allowNull: false
    },
  }, {});
  Logins.associate = function(models) {
    // associations can be defined here
  };
  return Logins;
};